<?php get_header(); ?>

<div class="row" role="main">
	<div class="col-xs-12 col-md-8">
		<?php if (have_posts()) : ?>
			<?php if (is_search()): ?>
				<?php get_template_part('content', 'search'); ?>
			<?php else: ?>
				<?php get_template_part('content', 'index'); ?>
			<?php endif; ?>
			<ul class="pager">
				<li class="previous"><?php next_posts_link( '<span class="glyphicon glyphicon-arrow-left"></span> Publica&ccedil;&otilde;es mais antigas', 0 ); ?></li>
				<li class="next"><?php previous_posts_link( 'Publica&ccedil;&otilde;es mais recentes <span class="glyphicon glyphicon-arrow-right"></span>' ); ?></li>
			</ul>
		<?php endif; ?>
	</div>

	<div class="col-xs-12 col-md-4">
		<?php get_sidebar(); ?>
	</div>
</div>

<?php get_footer(); ?>
