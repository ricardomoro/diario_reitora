<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form. The actual display of comments is
 * handled by a callback to twentyten_comment which is
 * located in the functions.php file.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

			<div id="comments">
<?php if ( post_password_required() ) : ?>
				<p class="nopassword"><?php _e( 'This post is password protected. Enter the password to view any comments.', 'twentyten' ); ?></p>
			</div><!-- #comments -->
<?php
		/* Stop the rest of comments.php from being processed,
		 * but don't kill the script entirely -- we still have
		 * to fully load the template.
		 */
		return;
	endif;
?>

<?php
	// You can start editing here -- including this comment!
?>

<?php if ( have_comments() ) : ?>
			<h3 id="comments-title"><?php
			printf( _n( 'Um coment&aacute;rio para %2$s', '%1$s coment&aacute;rios para %2$s', get_comments_number(), 'ifrswptheme' ),
			number_format_i18n( get_comments_number() ), '<em>' . get_the_title() . '</em>' );
			?></h3>

<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
			<div class="navigation">
				<div class="nav-previous"><?php previous_comments_link( __( '<span class="meta-nav">&larr;</span> Older Comments', 'twentyten' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( __( 'Newer Comments <span class="meta-nav">&rarr;</span>', 'twentyten' ) ); ?></div>
			</div> <!-- .navigation -->
<?php endif; // check for comment navigation ?>

			<ul class="commentlist list-group">
				<?php
					/* Loop through and list the comments. Tell wp_list_comments()
					 * to use twentyten_comment() to format the comments.
					 * If you want to overload this in a child theme then you can
					 * define twentyten_comment() and that will be used instead.
					 * See twentyten_comment() in twentyten/functions.php for more.
					 */
					wp_list_comments( array( 'callback' => 'ifrs_comment' ) );
				?>
			</ul>

<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
			<div class="navigation">
				<div class="nav-previous"><?php previous_comments_link( __( '<span class="meta-nav">&larr;</span> Older Comments', 'twentyten' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( __( 'Newer Comments <span class="meta-nav">&rarr;</span>', 'twentyten' ) ); ?></div>
			</div><!-- .navigation -->
<?php endif; // check for comment navigation ?>

	<?php
	/* If there are no comments and comments are closed, let's leave a little note, shall we?
	 * But we only want the note on posts and pages that had comments in the first place.
	 */
	if ( ! comments_open() && get_comments_number() ) : ?>
		<p class="nocomments"><?php _e( 'Coment&aacute;rios fechados.' , 'ifrswptheme' ); ?></p>
	<?php endif;  ?>

<?php endif; // end have_comments() ?>

<?php
	$fields =  array(
		'author' =>
		'<div class="form-group">
			<label for="author">' . __( 'Nome', 'ifrswptheme' ) . '</label> ' . ( $req ? '<span class="required">*</span>' : '' ) .
			'<input class="form-control" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' />
		</div>',

		'email' =>
		'<div class="form-group">
			<label for="email">' . __( 'Email', 'ifrswptheme' ) . '</label> ' . ( $req ? '<span class="required">*</span>' : '' ) .
			'<input class="form-control" id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' />
		</div>',

		'url' =>
		'<div class="form-group">
			<label for="url">' . __( 'Site', 'ifrswptheme' ) . '</label>' .
			'<input class="form-control" id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" />
		</div>',
	);

	$new_defaults = array(
		'title_reply' => 'Deixe um comentário',
		'title_reply_to' => 'Deixe um comentário para',
		'cancel_reply_link' => 'Cancelar resposta',
		'label_submit' => 'Enviar comentário',
		'comment_notes_before' =>
			'<p class="comment-notes">' .
				'Seu endereço de email não será publicado.' . '<br />' . ( $req ? 'Campos com asterisco (*) são obrigatórios.' : '' ) .
			'</p>',
		'fields' => apply_filters( 'comment_form_default_fields', $fields ),
		'comment_field' => 
			'<div class="form-group">
				<label for="comment">' . _x( 'Comment', 'noun' ) . '</label> ' . ( $req ? '<span class="required">*</span>' : '' ) .
				'<textarea class="form-control" id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>
			</div>',
		'comment_notes_after' => false,
	);
?>

<?php comment_form($new_defaults); ?>

</div><!-- #comments -->
