<div class="row">
	<form id="pesquisa" class="form-inline" method="get" action="<?php echo home_url( '/' ); ?>">
		<div class="col-xs-12">
			<div class="input-group">
				<input type="text" name="s" id="txtPesquisa" class="form-control" placeholder="Digite sua pesquisa aqui..." />
				<label for="txtPesquisa" class="sr-only">Pesquisa</label>
				<span class="input-group-btn">
					<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span><span class="sr-only">Pesquisar</span></button>
				</span>
			</div>
		</div>
	</form>
</div>