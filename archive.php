<?php get_header(); ?>

<div class="row" role="main">
	<div class="col-xs-12 col-md-8">
		<?php if (have_posts()) : ?>
			<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
			<?php if (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
				<h2>Arquivo</h2>
			<?php } ?>
			
			<?php get_template_part('content', 'archive'); ?>
			
			<ul class="pager">
				<li class="previous"><?php next_posts_link( '<span class="glyphicon glyphicon-arrow-left"></span> Publica&ccedil;&otilde;es anteriores', 0 ); ?></li>
				<li class="next"><?php previous_posts_link( 'Pr&oacute;ximas Publica&ccedil;&otilde;es <span class="glyphicon glyphicon-arrow-right"></span>' ); ?></li>
			</ul>
		<?php endif; ?>
	</div>

	<div class="col-xs-12 col-md-4">
		<?php get_sidebar(); ?>
	</div>
</div>

<?php get_footer(); ?>
