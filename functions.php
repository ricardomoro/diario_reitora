<?php
// Chama as funções de inicialização depois de certos eventos.
add_action( 'after_setup_theme', 'ifrs_setup' );
add_action( 'widgets_init', 'ifrs_widgets' );

// Adiciona filtros.
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
add_filter( 'excerpt_more', 'new_excerpt_more' );

function ifrs_setup() {
	// Registra uma localização de menu no tema.
	register_nav_menu( 'primary', __( 'Menu Principal', 'ifrs' ) );

	// Habilita o suporte a miniaturas nos posts.
	add_theme_support('post-thumbnails');
	// Define o tamanho das miniaturas.
	set_post_thumbnail_size(700, 0, false);
}

/**
 * Personaliza o tamanho (em palavras) do resumo dos posts.
 */
function custom_excerpt_length( $length ) {
	return 125;
}

/**
 * Personaliza o texto que vai ao final do resumo.
 */
function new_excerpt_more( $more ) {
	return '... <a class="leia-mais" href="' . get_permalink( get_the_ID() ) . '">(Leia mais<span class="sr-only"> sobre &ldquo;' . get_the_title( get_the_ID() ) . '&rdquo;</span>)</a>';
}

/**
 * Inicialização da Barra Lateral de widgets.
 */
function ifrs_widgets() {
	register_sidebar( array(
		'name' => __( 'Barra Lateral para Widgets', 'ifrswptheme' ),
		'id' => 'primary-widget-area',
		'description' => __( 'Adicione widgets aqui para aparecerem na barra lateral.', 'ifrs' ),
		'before_widget' => '<div id="%1$s" class="col-xs-12 %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );
}

/**
 * Template para comentários.
 */
function ifrs_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
	?>
	<li <?php comment_class('list-group-item'); ?> id="li-comment-<?php comment_ID(); ?>">
		<div id="comment-<?php comment_ID(); ?>">
			<div class="comment-author vcard">
				<?php echo get_avatar( $comment, 40 ); ?>
				<?php
					printf( '%s <span class="says">' . __( 'disse:', 'ifrs' ) . '</span>', sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) );
				?>
			</div><!-- .comment-author .vcard -->
			<?php if ( $comment->comment_approved == '0' ) : ?>
				<em class="comment-awaiting-moderation"><?php _e( 'Seu comentário está aguardando aprovação.', 'ifrs' ); ?></em>
				<br />
			<?php endif; ?>

			<div class="comment-meta"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
				<?php
					/* translators: 1: date, 2: time */
					printf( __( '%1$s &agrave;s %2$s', 'ifrs' ), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Editar)', 'ifrs' ), ' ' );
				?>
			</div><!-- .comment-meta -->

			<div class="comment-body"><?php comment_text(); ?></div>

			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			</div><!-- .reply -->
		</div><!-- #comment-##  -->

	<?php
			break;
		case 'pingback'  :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'ifrs' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( '(Editar)', 'ifrs' ), ' ' ); ?></p>
	<?php
			break;
	endswitch;
}

/**
 * Adiciona a funcionalidade de breadcrumbs automáticos para o tema.
 *
 * @c.bavota - http://bavotasan.com
 */
function ifrs_breadcrumbs() {
	if(!is_home()) {
		echo '<ol class="breadcrumb">';
		echo '<li><a href="'.home_url('/').'">'.get_bloginfo('name').'</a></li>';
		if (is_single()) {
			echo "<li>";
				the_category(' ', 'single');
			echo "</li>";
			echo "<li class='active'>";
				the_title();
			echo "</li>";
		} elseif (is_page()) {
			echo "<li class='active'>";
				the_title();
			echo "</li>";
		} elseif (is_category()) {
			$categories = get_the_category();
			foreach ($categories as $category) {
				echo "<li class='active'>";
					echo $category->name;
				echo "</li>";
			}
		} elseif (is_author()) {
			echo "<li class='active'>";
				echo "Publicações do autor";
			echo "</li>";
		} elseif (is_tag()) {
			echo "<li class='active'>";
				echo "Publica&ccedil;&otilde;es com a Tag &ldquo;" . single_tag_title() . "&rdquo;";
			echo "</li>";
		} elseif(is_day()) {
			echo "<li class='active'>";
				echo 'Publica&ccedil;&otilde;es em ' . get_the_time('j') . ' de ' . get_the_time('F') . ' de ' . get_the_time('Y');
			echo "</li>";
		} elseif (is_month()) {
			echo "<li class='active'>";
				echo 'Publica&ccedil;&otilde;es em ' . get_the_time('F') . ' de ' . get_the_time('Y');
			echo "</li>";
		} elseif (is_year()) {
			echo "<li class='active'>";
				echo 'Publica&ccedil;&otilde;es em ' . get_the_time('Y');
			echo "</li>";
		} elseif (is_search()) {
			echo '<li class="active">Resultados para a busca do termo "' . get_search_query() . '"</li>';
		}
		echo '</ol>';
	}
}
