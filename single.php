<?php get_header(); ?>

<div class="row" role="main">
	<div class="col-xs-12 col-md-8">
		<?php if (have_posts()) : ?>
			<?php get_template_part('content', 'single'); ?>
			<ul class="pager">
				<li class="previous"><?php previous_post_link('%link', '<span class="glyphicon glyphicon-arrow-left"></span> %title') ?></li>
				<li class="next"><?php next_post_link('%link', '%title <span class="glyphicon glyphicon-arrow-right"></span>') ?></li>
			</ul>
		<?php endif; ?>
	</div>

	<div class="col-xs-12 col-md-4">
		<?php get_sidebar(); ?>
	</div>
</div>

<?php get_footer(); ?>
