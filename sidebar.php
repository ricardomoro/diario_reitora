<aside class="row">
	<div class="panel panel-default">
		<div class="panel-body">
			<?php if (!dynamic_sidebar('primary-widget-area')) : ?>
				<div class="col-xs-12">
					<?php get_search_form(); ?>
				</div>
			<?php endif; ?>

			<div class="col-xs-12" id="rss">
				<a href="/feed/" title="Feed RSS do Site"><img src="<?php echo get_template_directory_uri(); ?>/img/rss.png" alt="Logo RSS"></a>
			</div>
		</div>
	</div>
</aside>
