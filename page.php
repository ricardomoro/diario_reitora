<?php get_header(); ?>

<div class="row" role="main">
	<div class="col-xs-12 col-md-8">
		<?php if (have_posts()) : ?>
			<?php while(have_posts()) : the_post(); ?>
				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<?php the_post_thumbnail('post-thumbnail', array('class' => 'img-responsive img-thumbnail')); ?>
				<?php the_content(); ?>
				<div class="clearfix"></div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>

	<div class="col-xs-12 col-md-4">
		<?php get_sidebar(); ?>
	</div>
</div>

<?php get_footer(); ?>
