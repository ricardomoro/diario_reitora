	<a href="#fim-conteudo" id="fim-conteudo" class="sr-only">Fim do conte&uacute;do</a>
</section><!-- /#principal -->

<!-- Rodapé -->
<footer role="contentinfo" class="container">
	<div class="row">
		<div id="link-para-topo" class="col-xs-12">
			<a href="#pular-para-conteudo">Topo da p&aacute;gina</a>
		</div>
	</div>
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="col-xs-12 col-sm-8 col-md-6">
					<address>
						<p><strong>Instituto Federal de Educa&ccedil;&atilde;o, Ci&ecirc;ncia e Tecnologia do Rio Grande do Sul</strong></p>
						<p><a href="http://www.ifrs.edu.br/" title="Site do IFRS">http://www.ifrs.edu.br/</a></p>
						<p>Email: <a href="mailto:gabinete@ifrs.edu.br">gabinete@ifrs.edu.br</a> | Ouvidoria: <a href="mailto:ouvidoria@ifrs.edu.br">ouvidoria@ifrs.edu.br</a></p>
						<p>Telefone: (54) 3449-3300 | Fax: (54) 3449-3333</p>
					</address>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-6">
					<a class="hidden-xs pull-right" href="<?php echo esc_url( __( 'http://www.wordpress.org/', 'ifrs' ) ); ?>" title="<?php esc_attr_e( 'Semantic Personal Publishing Platform', 'ifrs' ); ?>" target="_blank"><?php printf( __( 'Desenvolvido com %s', 'ifrs' ), 'WordPress' ); ?></a>
					<a class="visible-xs" href="<?php echo esc_url( __( 'http://www.wordpress.org/', 'ifrs' ) ); ?>" title="<?php esc_attr_e( 'Semantic Personal Publishing Platform', 'ifrs' ); ?>" target="_blank"><?php printf( __( 'Desenvolvido com %s', 'ifrs' ), 'WordPress' ); ?></a>

					<div class="clearfix"></div>

					<a class="hidden-xs pull-right" href="http://glyphicons.com/" target="_blank">Glyphicons</a>
					<a class="visible-xs" href="http://glyphicons.com/" target="_blank">Glyphicons</a>

					<div class="clearfix"></div>

					<br/>

					<a class="hidden-xs pull-right" href="http://bitbucket.org/ricardomoro/diario_reitora/" target="_blank">C&oacute;digo-fonte desse tema sob a licen&ccedil;a GPLv3</a>
					<a class="visible-xs" href="http://bitbucket.org/ricardomoro/diario_reitora/" target="_blank">C&oacute;digo-fonte desse tema sob a licen&ccedil;a GPLv3</a>
				</div>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
