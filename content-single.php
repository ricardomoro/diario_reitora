<?php while(have_posts()) : the_post(); ?>
	<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
	<p class="post-metadata">
		<span class="glyphicon glyphicon-time"></span> Publicado em <?php printf('%s de %s de %s &agrave;s %sh%smin', get_the_time('d'), get_the_time('F'), get_the_time('Y'), get_the_time('H'), get_the_time('i')) ?> por <?php the_author_posts_link(); ?> em &ldquo;<?php the_category(' &gt; '); ?>&rdquo;
		<?php if (get_the_modified_time() != get_the_time()): ?>
			<br />
			<span class="glyphicon glyphicon-refresh"></span> Atualizado pela &uacute;ltima vez em <?php printf('%s de %s de %s &agrave;s %sh%smin', get_the_modified_time('d'), get_the_modified_time('F'), get_the_modified_time('Y'), get_the_modified_time('H'), get_the_modified_time('i')); ?>
		<?php endif; ?>
	</p>
	<?php the_post_thumbnail('post-thumbnail', array('class' => 'img-responsive img-thumbnail')); ?>
	<?php the_content(); ?>
	<div class="clearfix"></div>
	<?php if(has_tag()): ?>
		<p class="post-tags">
			<span class="glyphicon glyphicon-tags"></span>&nbsp;<?php the_tags(''); ?>
		</p>
	<?php endif; ?>
	<hr/>
	<?php comments_template(); ?>
<?php endwhile; ?>
