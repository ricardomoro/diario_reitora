<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<!-- Meta-tags -->
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="description" content="<?php bloginfo( 'description' ); ?>">

	<!-- Título -->
	<title>
		<?php
		/*
		 * Print the <title> tag based on what is being viewed.
		 */
		global $page, $paged;

		wp_title( '|', true, 'right' );

		// Add the blog name.
		bloginfo( 'name' );

		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			echo " | $site_description";

		// Add a page number if necessary.
		if ( $paged >= 2 || $page >= 2 )
			echo ' | ' . sprintf( __( 'Página %s', 'ifrs' ), max( $paged, $page ) );

		?>
	</title>

	<!-- CSS -->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="screen" />

	<!-- Pingback -->
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<!-- JS -->
	<!--[if lt IE 9]>
		<script type="text/javascript" src="js/html5shiv.js"></script>
		<script type="text/javascript" src="js/respond.min.js"></script>
	<![endif]-->
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/wp8_ie10_bootstrap_hack.js"></script>
	<?php
		/* We add some JavaScript to pages with the comment form
		 * to support sites with threaded comments (when in use).
		 */
		if ( is_singular() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	?>

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<a href="#principal" id="pular-para-conteudo" class="sr-only">Pular direto para o conte&uacute;do</a>

	<!-- Barra do Governo -->
	<div id="barra-brasil">
		<div id="wrapper-barra-brasil">
			<div class="brasil-flag">
				<a href="http://brasil.gov.br" class="link-barra">Brasil</a>
			</div>
			<span class="acesso-info">
				<a href="http://brasil.gov.br/barra#acesso-informacao" class="link-barra">Acesso à informação</a>
			</span>
			<ul class="list">
				<li class="list-item first"><a href="http://brasil.gov.br/barra#participe" class="link-barra">Participe</a></li>
				<li class="list-item"><a href="http://www.servicos.gov.br/" class="link-barra">Serviços</a></li>
				<li class="list-item"><a href="http://www.planalto.gov.br/legislacao" class="link-barra">Legislação</a></li>
				<li class="list-item last last-item"><a href="http://brasil.gov.br/barra#orgaos-atuacao-canais" class="link-barra">Canais</a></li>
			</ul>
		</div>
	</div>

	<!-- Topo -->
	<header class="container">
		<div id="topo" class="row">
			<!-- Logotipo -->
			<div class="col-xs-12">
				<h1 class="sr-only"><?php bloginfo( 'name' ); ?></h1>
				<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/topo.png" alt="<?php bloginfo( 'name' ); ?> - Instituto Federal de Educação, Ciência e Tecnologia do Rio Grande do Sul" />
			</div>
		</div>

		<!-- Menu -->
		<div id="menu" class="row">
			<div class="col-xs-12">
				<a href="#inicio-menu" id="inicio-menu" class="sr-only">In&iacute;cio do menu</a>
				<nav class="navbar navbar-default" role="navigation">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
							<span class="sr-only">Mostrar/esconder menu</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<?php wp_nav_menu( array('theme_location' => 'primary', 'container' => 'div', 'container_class' => 'collapse navbar-collapse navbar-ex1-collapse', 'menu_class' => 'nav navbar-nav') ); ?>
				</nav>
				<a href="#fim-menu" id="fim-menu" class="sr-only">Fim do menu</a>
			</div>
		</div>

		<?php if (!is_home()): ?>
			<!-- Breadcrumb -->
			<div class="row">
				<div class="col-xs-12">
					<?php if (function_exists('ifrs_breadcrumbs')) ifrs_breadcrumbs(); ?>
				</div>
			</div>
		<?php endif; ?>
	</header>

	<!-- Corpo -->
	<section id="principal" class="container">
		<a href="#inicio-conteudo" id="inicio-conteudo" class="sr-only">In&iacute;cio do conte&uacute;do</a>
		