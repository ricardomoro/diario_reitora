<?php while(have_posts()) : the_post(); ?>
	<div class="post">
		<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<?php if ($post->post_type != 'page'): ?>
			<p class="post-metadata">
				<span class="glyphicon glyphicon-time"></span> <?php the_time('d/m/Y'); ?><br />
				<span class="glyphicon glyphicon-user"></span> <?php _e('Publicado por'); ?> <?php  the_author_posts_link(); ?> <?php _e('em') ?> &ldquo;<?php the_category(' &gt; ', 'single'); ?>&rdquo;
			</p>
		<?php endif; ?>
		<?php the_post_thumbnail('post-thumbnail', array('class' => 'img-responsive img-thumbnail')); ?>
		<?php the_excerpt(); ?>
		<div class="clearfix"></div>
		<?php if ($post->post_type != 'page'): ?>
			<p class="post-comment-link">
				<span class="glyphicon glyphicon-comment"></span> <?php comments_popup_link('Deixe um coment&aacute;rio', '1 coment&aacute;rio', '% coment&aacute;rios'); ?>
			</p>
		<?php endif; ?>
	</div>
	<hr/>
<?php endwhile; ?>
